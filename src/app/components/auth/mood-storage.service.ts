import { Injectable } from '@angular/core';
import { Mood } from 'src/app/models/mood';

const MOOD_KEY = 'MoodUser';

@Injectable({
  providedIn: 'root'
})
export class MoodStorageService {

  constructor() { }

public saveMood(moodSave:Mood) {

  window.sessionStorage.removeItem(MOOD_KEY); 
  window.sessionStorage.setItem(MOOD_KEY, JSON.stringify(moodSave)); 

}

public getSavedMood() : any {
  return JSON.parse(sessionStorage.getItem(MOOD_KEY));  
}

}



import { Component, OnInit } from '@angular/core';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { ViewportScroller } from '@angular/common';
import { Activity, ActivityService } from 'src/app/services/activity.service';
import { ActivatedRoute } from '@angular/router';
declare const scrollspy: any;

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss'],
})
export class ActivityComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private viewportScroller: ViewportScroller,
    private activityService: ActivityService
  ) {}

  public onClick(elementId: string): void {
    this.viewportScroller.scrollToAnchor(elementId);
  }

  public activitiesFiltered: any[];

  ngOnInit(): void {
    scrollspy();
    this.getActivity();
    
  }

  activity: Activity;

  getActivity() {
    var paramId = this.route.snapshot.paramMap.get('id');
    this.activityService.getActivity(paramId).subscribe((data) => {
      this.activity = data;
    });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Mood } from '../models/mood';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private userUrl = 'http://localhost:9980/api/test/user';
  private pmUrl = 'http://localhost:9980/api/test/pm';
  private adminUrl = 'http://localhost:9980/api/test/admin';
  getUserMoodURL = 'http://localhost:9980/api/auth/usermood';

  constructor(private http: HttpClient) {}

  getUserBoard(): Observable<string> {
    return this.http.get(this.userUrl, { responseType: 'text' });
  }

  getPMBoard(): Observable<string> {
    return this.http.get(this.pmUrl, { responseType: 'text' });
  }

  getAdminBoard(): Observable<string> {
    return this.http.get(this.adminUrl, { responseType: 'text' });
  }

  getUserMood(userId: string): Observable<Mood[]> {
    console.log(this.getUserMoodURL + '/' + userId);
    return this.http.get<Mood[]>(this.getUserMoodURL + '/' + userId);
  }
}

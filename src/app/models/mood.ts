import { NumberFormatStyle } from '@angular/common';

export class Mood {

  moodModel: string;
  budget: number;
  location: string;
  category: string;
  physicalMood: string;
  lastUpdate: string;
  
}

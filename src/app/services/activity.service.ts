import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


export class Activity {
  constructor(
    public id : string, 
    public name : string,
    public image : string, 
    public descriptionCourte : string, 
    public descriptionLongue : string, 
    public videoType1: string,
    public videoType2: string, 
    public videoType3: string 
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor(private httpClientActivity : HttpClient) { }

  getActivities() {
    return this.httpClientActivity.get<Activity[]>('http://localhost:8090/activity-api/activities')
  }

  getActivity(activityId? : string): Observable<Activity> {
    return this.httpClientActivity.get<Activity>('http://localhost:8090/activity-api/activity/' + activityId )
  }
}

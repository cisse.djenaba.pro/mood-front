# Diff Summary

Date : 2022-09-15 03:09:03

Directory c:\\Users\\cisse\\OneDrive\\Bureau\\Projet 4\\front\\mood

Total : 26 files,  356 codes, -12 comments, -66 blanks, all 278 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| HTML | 11 | 348 | -1 | -57 | 290 |
| TypeScript | 7 | 80 | 0 | 6 | 86 |
| JavaScript | 1 | 13 | 1 | 3 | 17 |
| SCSS | 7 | -85 | -12 | -18 | -115 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 26 | 356 | -12 | -66 | 278 |
| src | 26 | 356 | -12 | -66 | 278 |
| src\\app | 24 | 334 | -13 | -71 | 250 |
| src\\app\\components | 20 | 343 | -13 | -67 | 263 |
| src\\app\\components\\about-mood | 1 | 3 | 0 | -1 | 2 |
| src\\app\\components\\activity | 2 | 14 | 0 | 1 | 15 |
| src\\app\\components\\colorbook | 2 | 7 | 0 | 2 | 9 |
| src\\app\\components\\contact-form | 1 | 3 | 0 | 0 | 3 |
| src\\app\\components\\event | 3 | -109 | -13 | -24 | -146 |
| src\\app\\components\\header | 1 | -1 | 0 | 0 | -1 |
| src\\app\\components\\home | 2 | 35 | 0 | -3 | 32 |
| src\\app\\components\\images | 1 | 152 | 0 | -15 | 137 |
| src\\app\\components\\login-icon | 4 | 76 | 0 | 18 | 94 |
| src\\app\\components\\profile | 1 | 3 | 0 | 1 | 4 |
| src\\app\\components\\restapi | 2 | 160 | 0 | -46 | 114 |
| src\\app\\services | 1 | 4 | 0 | 0 | 4 |
| src\\assets | 1 | 13 | 1 | 3 | 17 |
| src\\assets\\js | 1 | 13 | 1 | 3 | 17 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)
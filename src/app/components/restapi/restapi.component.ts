import { Component, OnInit } from '@angular/core';
import { Weather } from 'src/app/models/weather.model';

import { faTachographDigital } from '@fortawesome/free-solid-svg-icons';
import { ApiService } from 'src/app/services/api.service';
import { EventModel } from 'src/app/models/events.model';
import { EventService } from 'src/app/services/event.service';
import { MoodService } from 'src/app/services/mood.service';
import { MoodStorageService } from '../auth/mood-storage.service';
import { Event } from 'src/app/services/event.service';
import { ActivityService } from 'src/app/services/activity.service';
import { Activity } from 'src/app/models/activity';
import { ActivatedRoute } from '@angular/router';
import { TokenStorageService } from '../auth/token-storage.service';
import { EventStorageServiceService } from 'src/app/services/event-storage-service.service';

declare function onLoadSetIcon(weather: any);

@Component({
  selector: 'app-restapi',
  templateUrl: './restapi.component.html',
  styleUrls: ['./restapi.component.scss'],
})
export class RestapiComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private moodStorageService: MoodStorageService,
    private moodService: MoodService,
    private api: ApiService,
    private service: EventService,
    private activityService: ActivityService,
    private tokenStorage: TokenStorageService,
    private token: TokenStorageService
  ) {}

  couvert: string = 'couvert';
  degage: string = 'dégagé';
  pluie: string = 'légère pluie';
  nuageux: string = 'partiellement nuageux';
  neige: string = 'neige';
  orage: string = 'orage';
  moodString: string = 'MOOD';

  gratuit: string = 'gratuit';
  payant: string = 'payant';

  sport: string = 'Sport';

  activity: Activity;

  roles: string[];
  authority: string;
  info: any;

  public eventFiltered: any[];
  public eventFiltered2: any[];
  public activitiesFiltered: any[];

  public eventsOut = [
    'Parc des Buttes Chaumont',
    'Bois de Boulogne',
    'Médiathèque de la Canopée la fontaine',
    'Jardin du Luxembourg',
    'Cyclofficine de Paris',
    'Eglise de la Madeleine',
    'La Cachette de Paris',
    'Place Martin Nadaud',
    'Château de Thoiry',
    'Jardin des méditations et des plantes médicinales',
    'Square Nadar',
    'Parc de la Villette',
    'La Pop',
    'Parc Montsouris',
    'La REcyclerie',
    'Bibliothèque Sorbier',
    'Berges de Seine',
    'Bassin de la Villette',
    'Bois de Vincennes',
    'Base de loisirs de Jablines',
    'Esplanade des Magasins Généraux (place de la Pointe)',
    'Montreuil',
    'Bagnolet, Métro Gallieni, sortie n°2',
    'Parc Georges Brassens',
    'Square Maurice Gardette',
    'Jardin partagé de Falbala',
    'Square Serge Regianni',
  ];

  public eventsMusique = ['Musique'];

  savedMood = this.moodStorageService.getSavedMood();
  savedEvents: Event[];

  searchText;

  dateTime: Date;

  ngOnInit(): void {
    this.getActivities().subscribe((data) => {
      this._activities = data;
      this.activitiesFiltered = this._activities;
      this.activityMoralPhysicalMood();
    });

    this.getEvents().subscribe(
      (data) => {
        this._events = data;
        this.eventFiltered = this._events;
        // this.service.saveEvents(this.eventFiltered);
        this.eventsMoralPhysicalMood();
        this.eventInOutMood();
      },
      (err) => console.log(err),
      () => {
        this.replacePrice();
      }
    );

    this.savedEvents = this.service.getSavedEvents();

    this.getWeathers();

    this.dateTime = new Date();

    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities(),
    };

    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every((role) => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        } else if (role === 'ROLE_PM') {
          this.authority = 'pm';
          return false;
        }
        this.authority = 'user';
        return true;
      });
    }
  }

  _weathers: Weather[] = [];
  public _events: EventModel[] = [];
  public _activities: Activity[] = [];

  getWeathers() {
    this.api.getWeathers().subscribe((data) => {
      this._weathers = data;
    });
  }

  addIcon() {}

  getEvents() {
    return this.service.getEvents();
  }

  getActivities() {
    return this.activityService.getActivities();
  }

  event: Event;

  getEvent() {
    var paramId = this.route.snapshot.paramMap.get('id');
    this.service.getEvent(paramId).subscribe((data) => {
      this.event = data;
    });
  }

  getActivity() {
    var paramId = this.route.snapshot.paramMap.get('id');
    this.activityService.getActivity(paramId).subscribe((data) => {
      this.activity = data;
    });
  }

  getFreeEvents() {
    this.eventFiltered = this._events.filter((element) => {
      return element.priceType === 'gratuit';
    });
  }

  getNotFreeEvents() {
    this.eventFiltered = this._events.filter((element) => {
      return element.priceType === 'payant';
    });
  }

  getEventsMusique() {
    this.eventFiltered = this._events.filter((element) => {
      return element.tags === 'Musique';
    });
  }

  getEventsCinema() {
    this.eventFiltered = this._events.filter((element) => {
      return element.tags === 'Cinéma';
    });
  }

  getEventsSport() {
    this.eventFiltered = this._events.filter((element) => {
      return element.tags === 'Sport';
    });
  }

  getEventsDanse() {
    this.eventFiltered = this._events.filter((element) => {
      return element.tags === 'Danse';
    });
  }

  getEventsTheatre() {
    this.eventFiltered = this._events.filter((element) => {
      return element.tags === 'Théâtre';
    });
  }

  getEventsBalade() {
    this.eventFiltered = this._events.filter((element) => {
      return element.tags === 'Balade';
    });
  }

  getEventsAtelier() {
    this.eventFiltered = this._events.filter((element) => {
      return element.tags === 'Atelier';
    });
  }

  eventsMoralPhysicalMood() {
    switch (this.savedMood.moodModel) {
      case 'MOOD_VERYBAD':
        if (this.savedMood.physicalMood === 'MOOD_1') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Cinéma' ||
              element.tags === 'BD;Cinéma;Conférence' ||
              element.tags === 'Cinéma;Conférence;Histoire' ||
              element.tags === 'Balade;Cinéma;Conférence' ||
              element.tags === 'Cinéma;Littérature;Musique' ||
              element.tags === 'Atelier;Cinéma;Conférence' ||
              element.tags === 'Cinéma;Innovation'
            );
          });
        }
        break;

      case 'MOOD_BAD':
        if (this.savedMood.physicalMood === 'MOOD_1') {
          this.eventFiltered = this._events.filter((element) => {
            return (
              element.tags === 'Musique' ||
              element.tags === 'Concert;Musique' ||
              element.tags === 'Spectacle musical' ||
              element.tags === 'Concerts;Danses;Théâtre' ||
              element.tags === 'Clubbing;Musique;Nature' ||
              element.tags === 'Danse;Musique;Spectacle musical' ||
              element.tags === 'Enfants;Musique;Spectacle musical' ||
              element.tags === 'Spectacle musical' ||
              element.tags === 'Concert;Musique;Spectacle musical' ||
              element.tags === 'Musique;Spectacle musical' ||
              element.tags === 'Enfants;Littérature;Spectacle musical'
            );
          });
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_2') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Musique' ||
              element.tags === 'Concert;Musique' ||
              element.tags === 'Spectacle musical' ||
              element.tags === 'Concerts;Danses;Théâtre' ||
              element.tags === 'Clubbing;Musique;Nature' ||
              element.tags === 'Danse;Musique;Spectacle musical' ||
              element.tags === 'Enfants;Musique;Spectacle musical' ||
              element.tags === 'Spectacle musical' ||
              element.tags === 'Concert;Musique;Spectacle musical' ||
              element.tags === 'Musique;Spectacle musical' ||
              element.tags === 'Enfants;Littérature;Spectacle musical'
            );
          });
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_3') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Balade' ||
              element.tags === 'Balade;Loisirs;Nature' ||
              element.tags === 'Balade;Nature;Sport' ||
              element.tags === 'Balade;Conférence;Peinture' ||
              element.tags === 'Balade;Conférence;Street-art' ||
              element.tags === 'Balade;Enfants' ||
              element.tags === 'Balade;Histoire;Loisirs' ||
              element.tags === 'Atelier;Balade;Nature'
            );
          });
          break;
        } else {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Sport' ||
              element.tags === 'Balade;Nature;Sport' ||
              element.tags === 'Atelier;Sport' ||
              element.tags === 'Loisirs;Sport' ||
              element.tags === 'Enfants;Nature;Sport' ||
              element.tags === 'Art contemporain;Expo;Sport' ||
              element.tags === 'Histoire;Solidarité;Sport'
            );
          });
        }
        break;

      case 'MOOD_NEUTRAL':
        if (this.savedMood.physicalMood === 'MOOD_1') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Théâtre' || element.tags === 'Enfants;Théâtre'
            );
          });
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_2') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Atelier' ||
              element.tags === 'Atelier;Conférence;Expo' ||
              element.tags === 'Atelier;Balade;Nature' ||
              element.tags === 'Atelier;Enfants;Sciences' ||
              element.tags === 'Atelier;Enfants;Loisirs' ||
              element.tags === 'Atelier;Loisirs' ||
              element.tags === 'Atelier;Littérature'
            );
          });
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_3') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Expo' ||
              element.tags === 'Atelier;Conférence;Expo' ||
              element.tags === 'Atelier;Expo;Théâtre' ||
              element.tags === 'Atelier;Expo;Sciences' ||
              element.tags === 'Atelier;Expo;Histoire' ||
              element.tags === 'Atelier;BD;Expo' ||
              element.tags === 'Atelier;Enfants;Expo' ||
              element.tags === 'Art contemporain;Atelier;Expo' ||
              element.tags === 'Atelier;Expo'
            );
          });
          break;
        } else {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Danse' ||
              element.tags === 'Clubbing;Danse;Humour' ||
              element.tags === 'Danse;Musique;Spectacle musical' ||
              element.tags === 'Clubbing;Danse;Musique' ||
              element.tags === 'Danse;Théâtre' ||
              element.tags === 'Danse;Musique' ||
              element.tags === 'Concert;Danse;Théâtre' ||
              element.tags === 'Danse;Enfants;Musique'
            );
          });
        }
        break;

      case 'MOOD_GOOD':
        if (this.savedMood.physicalMood === 'MOOD_1') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Expo' ||
              element.tags === 'Atelier;Conférence;Expo' ||
              element.tags === 'Atelier;Expo;Théâtre' ||
              element.tags === 'Atelier;Expo;Sciences' ||
              element.tags === 'Atelier;Expo;Histoire' ||
              element.tags === 'Atelier;BD;Expo' ||
              element.tags === 'Atelier;Enfants;Expo' ||
              element.tags === 'Art contemporain;Atelier;Expo' ||
              element.tags === 'Atelier;Expo'
            );
          });
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_2') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Littérature' ||
              element.tags === 'Littérature; Loisirs' ||
              element.tags === 'Atelier;Littérature'
            );
          });
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_3') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Concert' || element.tags === 'Concert;Musique'
            );
          });
          break;
        } else {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Balade;Nature;Sport' ||
              element.tags === 'Atelier;Sport' ||
              element.tags === 'Loisirs;Sport' ||
              element.tags === 'Enfants;Nature;Sport' ||
              element.tags === 'Art contemporain;Expo;Sport' ||
              element.tags === 'Histoire;Solidarité;Sport'
            );
          });
        }
        break;

      case 'MOOD_VERYGOOD':
        if (this.savedMood.physicalMood === 'MOOD_1') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Nature' ||
              element.tags === 'Balade;Loisirs;Nature' ||
              element.tags === 'Clubbing;Musique;Nature' ||
              element.tags === 'Conférence;Nature' ||
              element.tags === 'Loisirs;Nature' ||
              element.tags === 'Atelier;Balade;Nature' ||
              element.tags === 'Enfants;Nature;Sciences' ||
              element.tags === 'Atelier;Enfants;Nature' ||
              element.tags === 'Expo;Nature;Photo'
            );
          });
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_2') {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Nature' ||
              element.tags === 'Balade;Loisirs;Nature' ||
              element.tags === 'Clubbing;Musique;Nature' ||
              element.tags === 'Conférence;Nature' ||
              element.tags === 'Loisirs;Nature' ||
              element.tags === 'Atelier;Balade;Nature' ||
              element.tags === 'Enfants;Nature;Sciences' ||
              element.tags === 'Atelier;Enfants;Nature' ||
              element.tags === 'Expo;Nature;Photo'
            );
          });
          break;
        } else {
          this.eventFiltered = this.eventFiltered.filter((element) => {
            return (
              element.tags === 'Balade;Nature;Sport' ||
              element.tags === 'Atelier;Sport' ||
              element.tags === 'Loisirs;Sport' ||
              element.tags === 'Enfants;Nature;Sport' ||
              element.tags === 'Art contemporain;Expo;Sport' ||
              element.tags === 'Histoire;Solidarité;Sport'
            );
          });
        }
        break;

      default:
        console.log('Désolée, tu tombes dans le défaut');
    }
  }

  eventInOutMood() {
    switch (this.savedMood.location) {
      case 'MOOD_OUT':
        this.eventFiltered = this.eventFiltered.filter((element) => {
          return this.eventsOut.includes(element.place);
        });

        break;

      case 'MOOD_IDC':
        break;

      case 'MOOD_IN':
        this.eventFiltered = this.eventFiltered.filter((element) => {
          return !this.eventsOut.includes(element.place);
          //return this.eventsOut.reduce(element.place);
        });

        break;
    }
  }

  activityMoralPhysicalMood() {
    switch (this.savedMood.moodModel) {
      case 'MOOD_VERYBAD':
        this.activitiesFiltered = this.activitiesFiltered.filter((element) => {
          return element.name === 'Regarder un film';
        });
        break;

      case 'MOOD_BAD':
        if (this.savedMood.physicalMood === 'MOOD_1') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name == 'Ecouter de la musique';
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_2') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Faire de la méditation';
            }
          );
          break;
        } else {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Yoga';
            }
          );
        }
        break;

      case 'MOOD_NEUTRAL':
        if (this.savedMood.physicalMood === 'MOOD_1') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Lire un livre';
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_2') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Etirements';
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_3') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Afrofitness';
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_4') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Danser la Bokwa';
            }
          );
          break;
        } else {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Danser la Zumba';
            }
          );
        }
        break;

      case 'MOOD_GOOD':
        if (this.savedMood.physicalMood === 'MOOD_1') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Lire un livre';
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_2') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Etirements';
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_3') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return (
                element.name === 'Gym Suédoise' ||
                element.name === 'Yoga' ||
                element.name === 'Etirements' ||
                element.name == 'Ecouter de la musique'
              );
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_4') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Footing';
            }
          );
          break;
        } else {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Footing';
            }
          );
        }
        break;

      case 'MOOD_VERYGOOD':
        if (this.savedMood.physicalMood === 'MOOD_1') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Origami';
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_2') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Etirements';
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_3') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Gym Suédoise';
            }
          );
          break;
        }
        if (this.savedMood.physicalMood === 'MOOD_4') {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Vélo';
            }
          );
          break;
        } else {
          this.activitiesFiltered = this.activitiesFiltered.filter(
            (element) => {
              return element.name === 'Vélo';
            }
          );
        }
        break;

      default:
        console.log('Je rentre dans le switch case');
        break;
    }
  }

  replacePrice() {
    this.eventFiltered = this.eventFiltered.filter((event) => {
      if (event.price != undefined && event.priceType === 'payant') {
        let prices = [];
        prices.push(event.price.match(/\d+(\u002C\d{1,2})?/g));

        let firstPrice = parseInt(prices[0]);
        return firstPrice <= this.savedMood.budget;
      } else {
        return true;
      }
    });
  }
}

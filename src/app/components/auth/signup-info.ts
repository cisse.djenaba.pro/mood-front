export class SignUpInfo {
  userId: string;
  firstName: string;
  username: string;
  email: string;
  role: string[];
  password: string;

  constructor(
    userId: string,
    firstName: string,
    username: string,
    email: string,
    password: string
  ) {
    this.userId = userId;
    this.firstName = firstName;
    this.username = username;
    this.email = email;
    this.password = password;
    this.role = ['user'];
  }
}

export class EventModel {
  constructor(
    public _id: string,
    public apiId: string,
    public link: string,
    public title: string,
    public endDate: string,
    public startDate: string,
    public image: string,
    public tags: string,
    public description: string,
    public place: string,
    public address: string,
    public city: string,
    public zip: string,
    public contactUrl: string,
    public contactMail: string,
    public contactPhone: string,
    public priceType: string,
    public price: string,
    public texteReservationUrl: string
  ) {}
}
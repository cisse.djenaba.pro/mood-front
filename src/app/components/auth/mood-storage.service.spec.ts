import { TestBed } from '@angular/core/testing';

import { MoodStorageService } from './mood-storage.service';

describe('MoodStorageService', () => {
  let service: MoodStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoodStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

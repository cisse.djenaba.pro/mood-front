import { Component, OnInit } from '@angular/core';
import { Router } from 'express';
import { Mood } from 'src/app/models/mood';
import { MoodService } from 'src/app/services/mood.service';


@Component({
  selector: 'app-add-mood',
  templateUrl: './add-mood.component.html',
  styleUrls: ['./add-mood.component.scss']
})
export class AddMoodComponent implements OnInit {

 
  mood: Mood = new Mood(); 

  constructor(private moodService : MoodService) { }

  ngOnInit(): void {
  }

  addMood() {
    console.log(this.mood); 
    this.moodService.addMood(this.mood).subscribe(); 
    //<any>this.router.navigate(['/${mood}']); 
  }

}

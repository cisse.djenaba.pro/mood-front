# Diff Details

Date : 2022-09-15 03:09:03

Directory c:\\Users\\cisse\\OneDrive\\Bureau\\Projet 4\\front\\mood

Total : 26 files,  356 codes, -12 comments, -66 blanks, all 278 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/app/app.component.html](/src/app/app.component.html) | HTML | -10 | 0 | -3 | -13 |
| [src/app/app.component.scss](/src/app/app.component.scss) | SCSS | -5 | 0 | -1 | -6 |
| [src/app/app.module.ts](/src/app/app.module.ts) | TypeScript | 2 | 0 | 0 | 2 |
| [src/app/components/about-mood/about-mood.component.html](/src/app/components/about-mood/about-mood.component.html) | HTML | 3 | 0 | -1 | 2 |
| [src/app/components/activity/activity.component.scss](/src/app/components/activity/activity.component.scss) | SCSS | 14 | 0 | 0 | 14 |
| [src/app/components/activity/activity.component.ts](/src/app/components/activity/activity.component.ts) | TypeScript | 0 | 0 | 1 | 1 |
| [src/app/components/colorbook/colorbook.component.html](/src/app/components/colorbook/colorbook.component.html) | HTML | 6 | 0 | 2 | 8 |
| [src/app/components/colorbook/colorbook.component.scss](/src/app/components/colorbook/colorbook.component.scss) | SCSS | 1 | 0 | 0 | 1 |
| [src/app/components/contact-form/contact-form.component.html](/src/app/components/contact-form/contact-form.component.html) | HTML | 3 | 0 | 0 | 3 |
| [src/app/components/event/event.component.html](/src/app/components/event/event.component.html) | HTML | 0 | -1 | -4 | -5 |
| [src/app/components/event/event.component.scss](/src/app/components/event/event.component.scss) | SCSS | -114 | -12 | -22 | -148 |
| [src/app/components/event/event.component.ts](/src/app/components/event/event.component.ts) | TypeScript | 5 | 0 | 2 | 7 |
| [src/app/components/header/header.component.html](/src/app/components/header/header.component.html) | HTML | -1 | 0 | 0 | -1 |
| [src/app/components/home/home.component.html](/src/app/components/home/home.component.html) | HTML | 31 | 0 | -5 | 26 |
| [src/app/components/home/home.component.scss](/src/app/components/home/home.component.scss) | SCSS | 4 | 0 | 2 | 6 |
| [src/app/components/images/images.component.html](/src/app/components/images/images.component.html) | HTML | 152 | 0 | -15 | 137 |
| [src/app/components/login-icon/login-icon.component.html](/src/app/components/login-icon/login-icon.component.html) | HTML | 10 | 0 | 4 | 14 |
| [src/app/components/login-icon/login-icon.component.scss](/src/app/components/login-icon/login-icon.component.scss) | SCSS | 6 | 0 | 1 | 7 |
| [src/app/components/login-icon/login-icon.component.spec.ts](/src/app/components/login-icon/login-icon.component.spec.ts) | TypeScript | 18 | 0 | 6 | 24 |
| [src/app/components/login-icon/login-icon.component.ts](/src/app/components/login-icon/login-icon.component.ts) | TypeScript | 42 | 0 | 7 | 49 |
| [src/app/components/profile/profile.component.html](/src/app/components/profile/profile.component.html) | HTML | 3 | 0 | 1 | 4 |
| [src/app/components/restapi/restapi.component.html](/src/app/components/restapi/restapi.component.html) | HTML | 151 | 0 | -36 | 115 |
| [src/app/components/restapi/restapi.component.ts](/src/app/components/restapi/restapi.component.ts) | TypeScript | 9 | 0 | -10 | -1 |
| [src/app/services/event.service.ts](/src/app/services/event.service.ts) | TypeScript | 4 | 0 | 0 | 4 |
| [src/assets/js/app.js](/src/assets/js/app.js) | JavaScript | 13 | 1 | 3 | 17 |
| [src/styles.scss](/src/styles.scss) | SCSS | 9 | 0 | 2 | 11 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details
import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../auth/token-storage.service';

@Component({
  selector: 'app-colorbook',
  templateUrl: './colorbook.component.html',
  styleUrls: ['./colorbook.component.scss'],
})
export class ColorbookComponent implements OnInit {
  title = 'mood';
  roles: string[];
  authority: string;

  constructor(
    private tokenStorage: TokenStorageService,
    private token: TokenStorageService
  ) {}

  ngOnInit(): void {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities(),
    };

    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every((role) => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        } else if (role === 'ROLE_PM') {
          this.authority = 'pm';
          return false;
        }
        this.authority = 'user';
        return true;
      });
    }
  }

  info: any;
}

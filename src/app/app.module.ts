import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { YouTubePlayerModule } from "@angular/youtube-player";
import { CommonModule, registerLocaleData, ViewportScroller } from '@angular/common'; 
import * as fr from '@angular/common/locales/fr';
import { LOCALE_ID } from '@angular/core'; 
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { WeatherComponent } from './components/weather/weather.component';
import { RestapiComponent } from './components/restapi/restapi.component';

import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';

import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ColorbookComponent } from './components/colorbook/colorbook.component';
import { ImagesComponent } from './components/images/images.component';
import { EventComponent } from './components/event/event.component';
import { ActivityComponent } from './components/activity/activity.component';
import { CalendarCommonModule, CalendarModule, CalendarMonthModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiService } from './services/api.service';
import { CalendarComponent } from './components/calendar/calendar.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';
import { TrackerComponent } from './components/tracker/tracker.component';
import { HorizontalScrollDirective } from './horizontal-scroll.directive';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ContactService } from './services/contact.service';
import { AboutMoodComponent } from './components/about-mood/about-mood.component';
import { ProfileComponent } from './components/profile/profile.component';
import {
  TippyModule,
  tooltipVariation,
  popperVariation,
} from '@ngneat/helipopper';
import { LoginIconComponent } from './components/login-icon/login-icon.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WeatherComponent,
    RestapiComponent,
    RegisterComponent,
    UserComponent,
    LoginComponent,
    HomeComponent,
    ColorbookComponent,
    ImagesComponent,
    EventComponent,
    ActivityComponent,
    SignInComponent,
    CalendarComponent,
    TrackerComponent,
    HorizontalScrollDirective,
    ContactFormComponent,
    AboutMoodComponent,
    ProfileComponent,
    LoginIconComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    YouTubePlayerModule,
    NgbModalModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    CommonModule,
    Ng2SearchPipeModule,
    [
      TippyModule.forRoot({
        defaultVariation: 'tooltip',
        variations: {
          tooltip: tooltipVariation,
          popper: popperVariation,
        },
      }),
    ],
  ],
  providers: [
    ApiService,
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    ContactService,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor() {
    registerLocaleData(fr.default);
  }
}

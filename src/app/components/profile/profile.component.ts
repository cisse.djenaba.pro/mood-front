import { Component, OnInit } from '@angular/core';
import { Mood } from 'src/app/models/mood';
import { User } from 'src/app/services/http-client.service';
import { MoodService } from 'src/app/services/mood.service';
import { UserService } from 'src/app/services/user.service';
import { MoodStorageService } from '../auth/mood-storage.service';
import { TokenStorageService } from '../auth/token-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  constructor(
    private moodService: MoodService,
    private userService: UserService,
    private tokenStorageService: TokenStorageService,
    private token: TokenStorageService,
    private moodStorageService: MoodStorageService
  ) {}

  roles: string[];
  authority: string;
  _moods: Mood[] = [];
  savedMood = this.moodStorageService.getSavedMood();
  moodString: string = 'humeur';
  veryGood: string = 'MOOD_VERYGOOD';
  good: string = 'MOOD_GOOD';
  neutral: string = 'MOOD_NEUTRAL';
  bad: string = 'MOOD_BAD';
  veryBad: string = 'MOOD_VERYBAD';

  one: string = 'MOOD_1';
  two: string = 'MOOD_2';
  three: string = 'MOOD_3';
  four: string = 'MOOD_4';
  five: string = 'MOOD_5';

  in: string = 'MOOD_IN';
  out: string = 'MOOD_OUT';
  idc: string = 'MOOD_IDC';

  music: string = 'MOOD_MUSIC';
  meditation: string = 'MOOD_MEDITATION';
  sport: string = 'MOOD_SPORT';
  culture: string = 'MOOD_CULTURE';
  art: string = 'MOOD_ART';

  ngOnInit(): void {
    this.replaceMood();
    this.getUserMoods();
  }

  getUserMoods() {
    let userId = this.tokenStorageService.getUser();
    console.log('USERID' + userId.id);
    this.userService.getUserMood(userId.id).subscribe((data) => {
      this._moods = data;

      console.log('LISTE DE MOODS' + JSON.stringify(this._moods));
      console.log(data);
    });

    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities(),
    };

    if (this.tokenStorageService.getToken()) {
      this.roles = this.tokenStorageService.getAuthorities();
      this.roles.every((role) => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        } else if (role === 'ROLE_PM') {
          this.authority = 'pm';
          return false;
        }
        this.authority = 'user';
        return true;
      });
    }
  }

  info: any;

  replaceMood(): any {
    for (var i = 0; i < this._moods.length; i++) {
      if (this._moods[i].moodModel === 'MOOD_VERYGOOD') {
        return this._moods[i].moodModel === 'Très bonne humeur';
      } else {
        return console.log('Nothing Bruh');
      }
    }
  }

  replacePhysicalMood() {
    switch (this.savedMood.physicalMood) {
      case 'MOOD_1':
        return (this.savedMood.physicalMood = 'Très mauvaise');
      case 'MOOD_2':
        return (this.savedMood.physicalMood = 'Mauvaise');
      case 'MOOD_3':
        return (this.savedMood.physicalMood = 'Neutre');
      case 'MOOD_4':
        return (this.savedMood.physicalMood = 'Bonne');
      case 'MOOD_5':
        return (this.savedMood.physicalMood = 'Très bonne');
      default:
        return 'humeur';
    }
  }

  replaceLocationMood() {
    switch (this.savedMood.location) {
      case 'MOOD_IN':
        return (this.savedMood.location = "à rester à l'Intérieur");
      case 'MOOD_IDC':
        return (this.savedMood.location = 'indifférente au lieu');
      case 'MOOD_OUT':
        return (this.savedMood.location = "à profiter de l'Extérieur");

      default:
        return 'humeur';
    }
  }

  replaceCategoryMood() {
    switch (this.savedMood.category) {
      case 'MOOD_MUSIC':
        return (this.savedMood.category = 'musicale');
      case 'MOOD_SPORT':
        return (this.savedMood.category = 'sportive');
      case 'MOOD_MEDITATION':
        return (this.savedMood.category = 'méditative');
      case 'MOOD_CULTURE':
        return (this.savedMood.category = 'culturelle');
      case 'MOOD_ART':
        return (this.savedMood.category = 'artistique');
      default:
        return 'humeur';
    }
  }
}

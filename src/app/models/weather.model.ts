export class Weather {
  constructor(
    public id: string,
    public name: string,
    public zip: string,
    public lat: number,
    public lon: number,
    public weather_description: string,
    public temperature: number
  ) {}
}
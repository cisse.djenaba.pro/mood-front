import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { AuthLoginInfo } from '../auth/login-info';
import { TokenStorageService } from '../auth/token-storage.service';
import { HttpClientService, User } from 'src/app/services/http-client.service';
import { SignUpInfo } from '../auth/signup-info';
import { Mood } from 'src/app/models/mood';
import { Router } from '@angular/router';
import { MoodService } from 'src/app/services/mood.service';
import { MoodStorageService } from '../auth/mood-storage.service';



declare function onLoadMultistep();

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {

   
  info: any;
  mood: Mood = new Mood(); 

  constructor(private moodStorageService : MoodStorageService, private moodService : MoodService, private token: TokenStorageService, private router: Router) { }

   ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities(),
    };

   
  }

  addMood() {
    console.log(this.mood); 
    this.moodService.addMood(this.mood).subscribe(
          (data) => {
     this.moodStorageService.saveMood(this.mood);
     this.router.navigate(['/restapi']);
    }
    ); 
    //<any>this.router.navigate(['/${mood}']); 
  }



}

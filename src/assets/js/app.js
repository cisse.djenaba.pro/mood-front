window.onload = function () {
  //LOGIN SIGN-UP

  const switchers = [...document.querySelectorAll(".switcher")];

  switchers.forEach((item) => {
    item.addEventListener("click", function () {
      switchers.forEach((item) =>
        item.parentElement.classList.remove("is-active")
      );
      this.parentElement.classList.add("is-active");
    });
  });

  $(document).ready(function () {
    var base_color = "rgb(230,230,230)";
    var active_color = "orange";

    var child = 1;
    var length = $("section").length - 1;
    $("#prev").addClass("disabled");
    $("#submit").addClass("disabled");

    $("section").not("section:nth-of-type(1)").hide();
    $("section")
      .not("section:nth-of-type(1)")
      .css("transform", "translateX(100px)");

    var svgWidth = length * 50 + 24;
    $("#svg_wrap").html(
      '<svg version="1.1" id="svg_form_time" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 ' +
        svgWidth +
        ' 24" xml:space="preserve"></svg>'
    );

    function makeSVG(tag, attrs) {
      var el = document.createElementNS("http://www.w3.org/2000/svg", tag);
      for (var k in attrs) el.setAttribute(k, attrs[k]);
      return el;
    }

    for (i = 0; i < length; i++) {
      var positionX = 12 + i * 50;
      var rect = makeSVG("rect", { x: positionX, y: 2, width: 50, height: 1 });
      document.getElementById("svg_form_time").appendChild(rect);
      // <g><rect x="12" y="9" width="200" height="6"></rect></g>'
      var circle = makeSVG("circle", {
        cx: positionX,
        cy: 2,
        r: 2,
        width: positionX,
        height: 6,
      });
    }

    $("#svg_form_time rect").css("fill", base_color);
    $("circle:nth-of-type(1)").css("fill", active_color);

    $(".button").click(function () {
      $("#svg_form_time rect").css("fill", active_color);
      $("#svg_form_time circle").css("fill", active_color);
      var id = $(this).attr("id");
      if (id == "next") {
        $("#prev").removeClass("disabled");
        if (child >= length) {
          $(this).addClass("disabled");
          $("#submit").removeClass("disabled");
        }
        if (child <= length) {
          child++;
        }
      } else if (id == "prev") {
        $("#next").removeClass("disabled");
        $("#submit").addClass("disabled");
        if (child <= 2) {
          $(this).addClass("disabled");
        }
        if (child > 1) {
          child--;
        }
      }
      var circle_child = child + 1;
      $("#svg_form_time rect:nth-of-type(n + " + child + ")").css(
        "fill",
        base_color
      );
      $("#svg_form_time circle:nth-of-type(n + " + circle_child + ")").css(
        "fill",
        base_color
      );
      var currentSection = $("section:nth-of-type(" + child + ")");
      currentSection.fadeIn();
      currentSection.css("transform", "translateX(0)");
      currentSection.prevAll("section").css("transform", "translateX(-100px)");
      currentSection.nextAll("section").css("transform", "translateX(100px)");
      $("section").not(currentSection).hide();
    });
  });

  //MOOD INPUT

  document.querySelectorAll(".feedback li").forEach((entry) =>
    entry.addEventListener("click", (e) => {
      if (!entry.classList.contains("active")) {
        document
          .querySelector(".feedback li.active")
          .classList.remove("active");
        entry.classList.add("active");
      }
      e.preventDefault();
    })
  );
};

//TRACKER CALENDAR

function loadTracker() {
  var calendar = document.getElementById("calendar-table");
  var gridTable = document.getElementById("table-body");
  var currentDate = new Date();
  var selectedDate = currentDate;
  var selectedDayBlock = null;
  var globalEventObj = {};

  var sidebar = document.getElementById("sidebar");

  function createCalendar(date, side) {
    var currentDate = date;
    var startDate = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      1
    );

    var monthTitle = document.getElementById("month-name");
    var monthName = currentDate.toLocaleString("en-US", {
      month: "long",
    });
    var yearNum = currentDate.toLocaleString("en-US", {
      year: "numeric",
    });
    monthTitle.innerHTML = `${monthName} ${yearNum}`;

    if (side == "left") {
      gridTable.className = "animated fadeOutRight";
    } else {
      gridTable.className = "animated fadeOutLeft";
    }

    setTimeout(
      () => {
        gridTable.innerHTML = "";

        var newTr = document.createElement("div");
        newTr.className = "row";
        var currentTr = gridTable.appendChild(newTr);

        for (let i = 1; i < (startDate.getDay() || 7); i++) {
          let emptyDivCol = document.createElement("div");
          emptyDivCol.className = "col empty-day";
          currentTr.appendChild(emptyDivCol);
        }

        var lastDay = new Date(
          currentDate.getFullYear(),
          currentDate.getMonth() + 1,
          0
        );
        lastDay = lastDay.getDate();

        for (let i = 1; i <= lastDay; i++) {
          if (currentTr.children.length >= 7) {
            currentTr = gridTable.appendChild(addNewRow());
          }
          let currentDay = document.createElement("div");
          currentDay.className = "col";
          if (
            (selectedDayBlock == null && i == currentDate.getDate()) ||
            selectedDate.toDateString() ==
              new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                i
              ).toDateString()
          ) {
            selectedDate = new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              i
            );

            document.getElementById("eventDayName").innerHTML =
              selectedDate.toLocaleString("en-US", {
                month: "long",
                day: "numeric",
                year: "numeric",
              });

            selectedDayBlock = currentDay;
            setTimeout(() => {
              currentDay.classList.add("orange");
              currentDay.classList.add("lighten-3");
              currentDay.style["border-radius"] = "70px";
            }, 900);
          }
          currentDay.innerHTML = i;

          //show marks
          if (
            globalEventObj[
              new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                i
              ).toDateString()
            ]
          ) {
            let eventMark = document.createElement("div");
            eventMark.className = "day-mark";
            currentDay.appendChild(eventMark);
          }

          currentTr.appendChild(currentDay);
        }

        for (let i = currentTr.getElementsByTagName("div").length; i < 7; i++) {
          let emptyDivCol = document.createElement("div");
          emptyDivCol.className = "col empty-day";
          currentTr.appendChild(emptyDivCol);
        }

        if (side == "left") {
          gridTable.className = "animated fadeInLeft";
        } else {
          gridTable.className = "animated fadeInRight";
        }

        function addNewRow() {
          let node = document.createElement("div");
          node.className = "row";
          return node;
        }
      },
      !side ? 0 : 270
    );
  }

  createCalendar(currentDate);

  var todayDayName = document.getElementById("todayDayName");
  todayDayName.innerHTML =
    "Today is " +
    currentDate.toLocaleString("en-US", {
      weekday: "long",
      day: "numeric",
      month: "short",
    });

  var prevButton = document.getElementById("prev");
  var nextButton = document.getElementById("next");

  prevButton.onclick = function changeMonthPrev() {
    currentDate = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth() - 1
    );
    createCalendar(currentDate, "left");
  };
  nextButton.onclick = function changeMonthNext() {
    currentDate = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth() + 1
    );
    createCalendar(currentDate, "right");
  };

  function addEvent(title, desc) {
    if (!globalEventObj[selectedDate.toDateString()]) {
      globalEventObj[selectedDate.toDateString()] = {};
    }
    globalEventObj[selectedDate.toDateString()][title] = desc;
  }

  function showEvents() {
    let sidebarEvents = document.getElementById("sidebarEvents");
    let objWithDate = globalEventObj[selectedDate.toDateString()];

    sidebarEvents.innerHTML = "";

    if (objWithDate) {
      let eventsCount = 0;
      for (key in globalEventObj[selectedDate.toDateString()]) {
        let eventContainer = document.createElement("div");
        eventContainer.className = "eventCard";

        let eventHeader = document.createElement("div");
        eventHeader.className = "eventCard-header";

        let eventDescription = document.createElement("div");
        eventDescription.className = "eventCard-description";

        eventHeader.appendChild(document.createTextNode(key));
        eventContainer.appendChild(eventHeader);

        eventDescription.appendChild(document.createTextNode(objWithDate[key]));
        eventContainer.appendChild(eventDescription);

        let markWrapper = document.createElement("div");
        markWrapper.className = "eventCard-mark-wrapper";
        let mark = document.createElement("div");
        mark.classList = "eventCard-mark";
        markWrapper.appendChild(mark);
        eventContainer.appendChild(markWrapper);

        sidebarEvents.appendChild(eventContainer);

        eventsCount++;
      }
      let emptyFormMessage = document.getElementById("emptyFormTitle");
      emptyFormMessage.innerHTML = `${eventsCount} events now`;
    } else {
      let emptyMessage = document.createElement("div");
      emptyMessage.className = "empty-message";
      emptyMessage.innerHTML =
        "Il n'y a pas d'éléments enregistrés pour cette date.";
      sidebarEvents.appendChild(emptyMessage);
      let emptyFormMessage = document.getElementById("emptyFormTitle");
      emptyFormMessage.innerHTML = "No events now";
    }
  }

  gridTable.onclick = function (e) {
    if (
      !e.target.classList.contains("col") ||
      e.target.classList.contains("empty-day")
    ) {
      return;
    }

    if (selectedDayBlock) {
      if (
        selectedDayBlock.classList.contains("orange") &&
        selectedDayBlock.classList.contains("lighten-3")
      ) {
        selectedDayBlock.classList.remove("orange");
        selectedDayBlock.classList.remove("lighten-3");
      }
    }
    selectedDayBlock = e.target;
    selectedDayBlock.classList.add("orange");
    selectedDayBlock.classList.add("lighten-3");
    selectedDayBlock.style["border-radius"] = "70px";

    selectedDate = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      parseInt(e.target.innerHTML)
    );

    showEvents();

    document.getElementById("eventDayName").innerHTML =
      selectedDate.toLocaleString("en-US", {
        month: "long",
        day: "numeric",
        year: "numeric",
      });
  };

  var changeFormButton = document.getElementById("changeFormButton");
  var addForm = document.getElementById("addForm");
  changeFormButton.onclick = function (e) {
    addForm.style.top = 0;
  };

  var cancelAdd = document.getElementById("cancelAdd");
  cancelAdd.onclick = function (e) {
    addForm.style.top = "100%";
    let inputs = addForm.getElementsByTagName("input");
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = "";
    }
    let labels = addForm.getElementsByTagName("label");
    for (let i = 0; i < labels.length; i++) {
      labels[i].className = "";
    }
  };

  var addEventButton = document.getElementById("addEventButton");
  addEventButton.onclick = function (e) {
    let title = document.getElementById("eventTitleInput").value.trim();
    let desc = document.getElementById("eventDescInput").value.trim();

    if (!title || !desc) {
      document.getElementById("eventTitleInput").value = "";
      document.getElementById("eventDescInput").value = "";
      let labels = addForm.getElementsByTagName("label");
      for (let i = 0; i < labels.length; i++) {
        labels[i].className = "";
      }
      return;
    }

    addEvent(title, desc);
    showEvents();

    if (!selectedDayBlock.querySelector(".day-mark")) {
      selectedDayBlock.appendChild(document.createElement("div")).className =
        "day-mark";
    }

    let inputs = addForm.getElementsByTagName("input");
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = "";
    }
    let labels = addForm.getElementsByTagName("label");
    for (let i = 0; i < labels.length; i++) {
      labels[i].className = "";
    }
  };

  var calendar = document.getElementById("calendar-table");
  var gridTable = document.getElementById("table-body");
  var currentDate = new Date();
  var selectedDate = currentDate;
  var selectedDayBlock = null;
  var globalEventObj = {};

  var sidebar = document.getElementById("sidebar");

  function createCalendar(date, side) {
    var currentDate = date;
    var startDate = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      1
    );

    var monthTitle = document.getElementById("month-name");
    var monthName = currentDate.toLocaleString("fr-FR", {
      month: "long",
    });
    var yearNum = currentDate.toLocaleString("fr-FR", {
      year: "numeric",
    });
    monthTitle.innerHTML = `${monthName} ${yearNum}`;

    if (side == "left") {
      gridTable.className = "animated fadeOutRight";
    } else {
      gridTable.className = "animated fadeOutLeft";
    }

    setTimeout(
      () => {
        gridTable.innerHTML = "";

        var newTr = document.createElement("div");
        newTr.className = "row";
        var currentTr = gridTable.appendChild(newTr);

        for (let i = 1; i < (startDate.getDay() || 7); i++) {
          let emptyDivCol = document.createElement("div");
          emptyDivCol.className = "col empty-day";
          currentTr.appendChild(emptyDivCol);
        }

        var lastDay = new Date(
          currentDate.getFullYear(),
          currentDate.getMonth() + 1,
          0
        );
        lastDay = lastDay.getDate();

        for (let i = 1; i <= lastDay; i++) {
          if (currentTr.children.length >= 7) {
            currentTr = gridTable.appendChild(addNewRow());
          }
          let currentDay = document.createElement("div");
          currentDay.className = "col";
          if (
            (selectedDayBlock == null && i == currentDate.getDate()) ||
            selectedDate.toDateString() ==
              new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                i
              ).toDateString()
          ) {
            selectedDate = new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              i
            );

            document.getElementById("eventDayName").innerHTML =
              selectedDate.toLocaleString("fr-FR", {
                month: "long",
                day: "numeric",
                year: "numeric",
              });

            selectedDayBlock = currentDay;
            setTimeout(() => {
              currentDay.classList.add("orange");
              currentDay.classList.add("lighten-3");
              currentDay.style["border-radius"] = "70px";
            }, 900);
          }
          currentDay.innerHTML = i;

          //show marks
          if (
            globalEventObj[
              new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                i
              ).toDateString()
            ]
          ) {
            let eventMark = document.createElement("div");
            eventMark.className = "day-mark";
            currentDay.appendChild(eventMark);
          }

          currentTr.appendChild(currentDay);
        }

        for (let i = currentTr.getElementsByTagName("div").length; i < 7; i++) {
          let emptyDivCol = document.createElement("div");
          emptyDivCol.className = "col empty-day";
          currentTr.appendChild(emptyDivCol);
        }

        if (side == "left") {
          gridTable.className = "animated fadeInLeft";
        } else {
          gridTable.className = "animated fadeInRight";
        }

        function addNewRow() {
          let node = document.createElement("div");
          node.className = "row";
          return node;
        }
      },
      !side ? 0 : 270
    );
  }

  createCalendar(currentDate);

  var todayDayName = document.getElementById("todayDayName");
  todayDayName.innerHTML =
    "Aujourd'hui nous sommes, " +
    currentDate.toLocaleString("fr-FR", {
      weekday: "long",
      day: "numeric",
      month: "short",
    });

  var prevButton = document.getElementById("prev");
  var nextButton = document.getElementById("next");

  prevButton.onclick = function changeMonthPrev() {
    currentDate = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth() - 1
    );
    createCalendar(currentDate, "left");
  };
  nextButton.onclick = function changeMonthNext() {
    currentDate = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth() + 1
    );
    createCalendar(currentDate, "right");
  };

  function addEvent(title, desc) {
    if (!globalEventObj[selectedDate.toDateString()]) {
      globalEventObj[selectedDate.toDateString()] = {};
    }
    globalEventObj[selectedDate.toDateString()][title] = desc;
  }

  function showEvents() {
    let sidebarEvents = document.getElementById("sidebarEvents");
    let objWithDate = globalEventObj[selectedDate.toDateString()];

    sidebarEvents.innerHTML = "";

    if (objWithDate) {
      let eventsCount = 0;
      for (key in globalEventObj[selectedDate.toDateString()]) {
        let eventContainer = document.createElement("div");
        eventContainer.className = "eventCard";

        let eventHeader = document.createElement("div");
        eventHeader.className = "eventCard-header";

        let eventDescription = document.createElement("div");
        eventDescription.className = "eventCard-description";

        eventHeader.appendChild(document.createTextNode(key));
        eventContainer.appendChild(eventHeader);

        eventDescription.appendChild(document.createTextNode(objWithDate[key]));
        eventContainer.appendChild(eventDescription);

        let markWrapper = document.createElement("div");
        markWrapper.className = "eventCard-mark-wrapper";
        let mark = document.createElement("div");
        mark.classList = "eventCard-mark";
        markWrapper.appendChild(mark);
        eventContainer.appendChild(markWrapper);

        sidebarEvents.appendChild(eventContainer);

        eventsCount++;
      }
      let emptyFormMessage = document.getElementById("emptyFormTitle");
      emptyFormMessage.innerHTML = `${eventsCount} events now`;
    } else {
      let emptyMessage = document.createElement("div");
      emptyMessage.className = "empty-message";
      emptyMessage.innerHTML =
        "Il n'y a pas d'éléments enregistrés pour cette date.";
      sidebarEvents.appendChild(emptyMessage);
      let emptyFormMessage = document.getElementById("emptyFormTitle");
      emptyFormMessage.innerHTML = "No events now";
    }
  }

  gridTable.onclick = function (e) {
    if (
      !e.target.classList.contains("col") ||
      e.target.classList.contains("empty-day")
    ) {
      return;
    }

    if (selectedDayBlock) {
      if (
        selectedDayBlock.classList.contains("orange") &&
        selectedDayBlock.classList.contains("lighten-3")
      ) {
        selectedDayBlock.classList.remove("orange");
        selectedDayBlock.classList.remove("lighten-3");
      }
    }
    selectedDayBlock = e.target;
    selectedDayBlock.classList.add("orange");
    selectedDayBlock.classList.add("lighten-3");
    selectedDayBlock.style["border-radius"] = "70px";

    selectedDate = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      parseInt(e.target.innerHTML)
    );

    showEvents();

    document.getElementById("eventDayName").innerHTML =
      selectedDate.toLocaleString("fr-FR", {
        month: "long",
        day: "numeric",
        year: "numeric",
      });
  };

  var changeFormButton = document.getElementById("changeFormButton");
  var addForm = document.getElementById("addForm");
  changeFormButton.onclick = function (e) {
    addForm.style.top = 0;
  };

  var cancelAdd = document.getElementById("cancelAdd");
  cancelAdd.onclick = function (e) {
    addForm.style.top = "100%";
    let inputs = addForm.getElementsByTagName("input");
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = "";
    }
    let labels = addForm.getElementsByTagName("label");
    for (let i = 0; i < labels.length; i++) {
      labels[i].className = "";
    }
  };

  var addEventButton = document.getElementById("addEventButton");
  addEventButton.onclick = function (e) {
    let title = document.getElementById("eventTitleInput").value.trim();
    let desc = document.getElementById("eventDescInput").value.trim();

    if (!title || !desc) {
      document.getElementById("eventTitleInput").value = "";
      document.getElementById("eventDescInput").value = "";
      let labels = addForm.getElementsByTagName("label");
      for (let i = 0; i < labels.length; i++) {
        labels[i].className = "";
      }
      return;
    }

    addEvent(title, desc);
    showEvents();

    if (!selectedDayBlock.querySelector(".day-mark")) {
      selectedDayBlock.appendChild(document.createElement("div")).className =
        "day-mark";
    }

    let inputs = addForm.getElementsByTagName("input");
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = "";
    }
    let labels = addForm.getElementsByTagName("label");
    for (let i = 0; i < labels.length; i++) {
      labels[i].className = "";
    }
  };
}

//ACTIVTY SCROLLSPY

function scrollspy() {
  console.log("Je rentre dans le scrollspy");

  document.addEventListener("DOMContentLoaded", function () {
    const introContainer = document.querySelector(".intro");
    const videoContainer = introContainer.querySelector(".popout-video");
    const video = videoContainer.querySelector("video");
    let videoHeight = videoContainer.offsetHeight;

    const closeVideoBtn = document.querySelector(".close-video");

    let popOut = true;

    introContainer.style.height = `${videoHeight}px`;

    window.addEventListener("scroll", function (e) {
      if (window.scrollY > videoHeight) {
        // only pop out the video if it wasnt closed before
        if (popOut) {
          videoContainer.classList.add("popout-video--popout");
          // set video container off the screen for the slide in animation
          videoContainer.style.top = `-${videoHeight}px`;
        }
      } else {
        videoContainer.classList.remove("popout-video--popout");
        videoContainer.style.top = `0px`;
        popOut = true;
      }
    });

    // close the video and prevent from opening again on scrolling + pause the video
    closeVideoBtn.addEventListener("click", function () {
      videoContainer.classList.remove("popout-video--popout");
      video.pause();
      popOut = false;
    });

    window.addEventListener("resize", function () {
      videoHeight = videoContainer.offsetHeight;
      introContainer.style.height = `${videoHeight}px`;
    });
  });
}

//EVENT

function eventAnim() {
  $(document).ready(function ($) {
    console.clear();

    ScrollOut({
      cssProps: {
        visibleY: true,
        viewportY: true,
        scrollPercentY: true,
      },
      threshold: 0.2,
    });
  });
}

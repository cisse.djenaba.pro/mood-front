import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HeaderComponent } from './components/header/header.component';
import { RestapiComponent } from './components/restapi/restapi.component';
import { RegisterComponent } from './components/register/register.component';
import { WeatherComponent } from './components/weather/weather.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ImagesComponent } from './components/images/images.component';
import { EventComponent } from './components/event/event.component';
import { ActivityComponent } from './components/activity/activity.component';
import { ColorbookComponent } from './components/colorbook/colorbook.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { TrackerComponent } from './components/tracker/tracker.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { AboutMoodComponent } from './components/about-mood/about-mood.component';
import { ProfileComponent } from './components/profile/profile.component';


const routes: Routes = [
  { path: 'weather', component: WeatherComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'restapi', component: RestapiComponent },
  { path: 'user', component: UserComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'colorbook', component: ColorbookComponent },
  { path: 'images', component: ImagesComponent },
  { path: 'event/:id', component: EventComponent}, 
  { path: 'activity/:id', component: ActivityComponent},
  { path: "sign-in", component : SignInComponent},
  { path: 'calendar', component : CalendarComponent}, 
  { path: 'tracker', component : TrackerComponent},
  { path: 'contact', component : ContactFormComponent},
  { path: 'about-mood', component : AboutMoodComponent},
  { path: 'profile', component : ProfileComponent}, 
  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { anchorScrolling: 'enabled'})],
  exports: [RouterModule],
})


export class AppRoutingModule {}

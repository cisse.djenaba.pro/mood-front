export class MoodInfo {
    
    moodModel : string;
    budget : number; 
    location : string; 
    category : string; 
    physicalMood: string; 

    constructor(moodModel : string, budget : number, location : string, category : string, physicalMood: string ) {
        
        this.moodModel = moodModel; 
        this.budget = budget ; 
        this.location = location ; 
        this.physicalMood = physicalMood ; 
        this.category = category ;  
           
    }
}

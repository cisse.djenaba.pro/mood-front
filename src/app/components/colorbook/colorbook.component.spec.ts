import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorbookComponent } from './colorbook.component';

describe('ColorbookComponent', () => {
  let component: ColorbookComponent;
  let fixture: ComponentFixture<ColorbookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorbookComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColorbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

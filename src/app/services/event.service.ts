import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const EVENT_KEY = 'Events';

export class Event {
  constructor(
    public _id: string,
    public apiId: string,
    public link: string,
    public title: string,
    public endDate: string,
    public startDate: string,
    public image: string,
    public tags: string,
    public description: string,
    public place: string,
    public address: string,
    public city: string,
    public zip: string,
    public contactUrl: string,
    public contactMail: string,
    public contactPhone: string,
    public priceType: string,
    public price: string,
    public texteReservationUrl: string,
    public contactFacebook: string,
    public transport: string,
    public leadText: string,
    public dateDescription: string
  ) {}
}

@Injectable({
  providedIn: 'root',
})
export class EventService {
  constructor(private httpClientEvent: HttpClient) {}

  getEvents() {
    return this.httpClientEvent.get<Event[]>(
      'http://localhost:8090/event-api/events'
    );
  }

  getEvent(eventId?: string): Observable<Event> {
    return this.httpClientEvent.get<Event>(
      'http://localhost:8090/event-api/event/' + eventId
    );
  }

  public saveEvents(eventsList: Event[]) {
    window.localStorage.setItem(EVENT_KEY, JSON.stringify(eventsList));
    console.log('EVENTLIST' + JSON.stringify(eventsList));
  }

  public getSavedEvents(): any {
    var eventsJson = window.localStorage.getItem(EVENT_KEY);
    return JSON.parse(eventsJson);
  }
}

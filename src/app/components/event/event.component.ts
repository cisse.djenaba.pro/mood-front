import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CalendarEventActionsComponent } from 'angular-calendar/modules/common/calendar-event-actions.component';
import { EventService } from 'src/app/services/event.service';
import { Event } from 'src/app/services/event.service';

declare const eventAnim: any;

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
})
export class EventComponent implements OnInit {
  constructor(
    private eventService: EventService,
    private route: ActivatedRoute
  ) {}

  gratuit: string = 'gratuit';
  payant: string = 'payant';

  ngOnInit(): void {
    eventAnim();
    this.getEvent();
  }

  event: Event;

  getEvent() {
    var paramId = this.route.snapshot.paramMap.get('id');
    this.eventService.getEvent(paramId).subscribe((data) => {
      this.event = data;
    });
  }
}

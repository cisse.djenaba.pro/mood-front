# Summary

Date : 2022-09-15 03:09:03

Directory c:\\Users\\cisse\\OneDrive\\Bureau\\Projet 4\\front\\mood

Total : 134 files,  43950 codes, 280 comments, 1624 blanks, all 45854 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 5 | 33,349 | 2 | 7 | 33,358 |
| SCSS | 20 | 4,596 | 134 | 575 | 5,305 |
| TypeScript | 82 | 2,786 | 86 | 599 | 3,471 |
| HTML | 20 | 1,775 | 5 | 187 | 1,967 |
| JavaScript | 5 | 1,401 | 52 | 240 | 1,693 |
| JSON with Comments | 1 | 29 | 1 | 2 | 32 |
| Markdown | 1 | 14 | 0 | 14 | 28 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 134 | 43,950 | 280 | 1,624 | 45,854 |
| src | 125 | 10,521 | 256 | 1,599 | 12,376 |
| src\\app | 115 | 9,082 | 162 | 1,329 | 10,573 |
| src\\app\\components | 83 | 8,366 | 144 | 1,152 | 9,662 |
| src\\app\\components\\about-mood | 4 | 54 | 0 | 17 | 71 |
| src\\app\\components\\activity | 4 | 623 | 6 | 45 | 674 |
| src\\app\\components\\auth | 11 | 216 | 0 | 73 | 289 |
| src\\app\\components\\calendar | 4 | 387 | 1 | 39 | 427 |
| src\\app\\components\\colorbook | 4 | 184 | 0 | 30 | 214 |
| src\\app\\components\\contact-form | 4 | 292 | 4 | 52 | 348 |
| src\\app\\components\\event | 4 | 202 | 0 | 34 | 236 |
| src\\app\\components\\header | 4 | 297 | 6 | 53 | 356 |
| src\\app\\components\\home | 4 | 672 | 4 | 72 | 748 |
| src\\app\\components\\images | 4 | 616 | 13 | 87 | 716 |
| src\\app\\components\\login | 4 | 324 | 28 | 63 | 415 |
| src\\app\\components\\login-icon | 4 | 76 | 0 | 18 | 94 |
| src\\app\\components\\profile | 4 | 284 | 4 | 38 | 326 |
| src\\app\\components\\register | 4 | 340 | 8 | 65 | 413 |
| src\\app\\components\\restapi | 4 | 1,969 | 44 | 156 | 2,169 |
| src\\app\\components\\sign-in | 4 | 700 | 8 | 127 | 835 |
| src\\app\\components\\tracker | 4 | 462 | 2 | 115 | 579 |
| src\\app\\components\\user | 4 | 46 | 0 | 12 | 58 |
| src\\app\\components\\weather | 4 | 622 | 16 | 56 | 694 |
| src\\app\\models | 7 | 69 | 0 | 13 | 82 |
| src\\app\\services | 16 | 363 | 1 | 108 | 472 |
| src\\assets | 3 | 1,364 | 31 | 238 | 1,633 |
| src\\assets\\js | 3 | 1,364 | 31 | 238 | 1,633 |
| src\\environments | 2 | 9 | 11 | 4 | 24 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class User {
  constructor(
    public id: string,
    public firstName: string,
    public password: string,
    public email: string
  ) {}
}

export class User2 {
  constructor() {}
}

export class Weather {
  constructor(
    public id: string,
    public name: string,
    public zip: string,
    public lat: number,
    public lon: number,
    public weather_description: string,
    public temperature: number
  ) {}
}

// This is a service class
@Injectable({
  providedIn: 'root',
})
export class HttpClientService {
  constructor(private httpClient: HttpClient) {}

  getUsers() {
    console.log('Test Call');
    return this.httpClient.get<User[]>('http://localhost:9984/user/users');
  }

  getWeathers() {
    console.log('Test Call');
    return this.httpClient.get<Weather[]>(
      'http://localhost:8090/weather-api/weathers'
    );
  }
}

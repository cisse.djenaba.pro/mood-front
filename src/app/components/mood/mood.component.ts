import { Component, OnInit } from '@angular/core';

import { MoodInfo } from '../auth/mood-info';
import { AuthService } from '../auth/auth.service';
import { Mood } from 'src/app/models/mood';
import { MoodService } from 'src/app/services/mood.service';


@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.scss']
})
export class MoodComponent implements OnInit {


  mood : Mood[]; 

 
  constructor(private moodService : MoodService) { }

  ngOnInit(): void { 
    /*this.mood = 
    [
      {
      moodModel : "MOOD_BAD"
    }, 
  { moodModel: "MOOD_GOOD"}];*/
  this.getAllMood(); 
  }
 
  getAllMood() {
    this.moodService.getAllMood().subscribe(data =>{
      this.mood = data ; 
    })
  }

 
/*
  onSubmit() {
    console.log(this.form);

    this.moodInfo = new MoodInfo(
      this.form.mood);

      this.authService.registerMood(this.moodInfo).subscribe(
      data => {
        console.log(data);
        this.isSignedUp = true;
        this.isSignUpFailed = false;
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
*/

}

import { Component, OnInit } from '@angular/core';
import { EventModel } from 'src/app/models/events.model';
import { Event, EventService } from 'src/app/services/event.service';
import { HttpClientService } from 'src/app/services/http-client.service';

@Component({
  selector: 'app-list-events',
  templateUrl: './list-events.component.html',
  styleUrls: ['./list-events.component.scss']
})


export class ListEventsComponent implements OnInit {

 gratuit : string = 'gratuit';
 payant : string = 'payant'; 

 searchText;

  constructor(private service : EventService) { }

  ngOnInit(): void {
    this.getEvents();
  
  }

 
  _events : EventModel[] = []; 


  getEvents() {
    this.service.getEvents().subscribe((data) => {
      this._events = data;
    });
  }


  getFreeEvents() {
    this._events.filter((element) => {
       element.priceType = "gratuit";
    })
  }

   getNotFreeEvents() {
    this._events.filter((element) => {
       element.priceType = "payant";
    })
  }

  getSearchedEvent() {
    let value = ""; 
    this._events.filter((element) => {
      element.title.includes(value); 
    })
  }

}



import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutMoodComponent } from './about-mood.component';

describe('AboutMoodComponent', () => {
  let component: AboutMoodComponent;
  let fixture: ComponentFixture<AboutMoodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutMoodComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AboutMoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

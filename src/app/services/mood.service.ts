import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../components/auth/token-storage.service';
import { Mood } from '../models/mood';
import { User } from './http-client.service';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class MoodService {


  baseURL = "http://localhost:9980/api/auth/moods";
  otherURL = "http://localhost:9980/api/auth/addmood2"; 
  getUserMoodURL = "http://localhost:9980/api/auth/usermood"
  
  constructor(private httpClient : HttpClient, private token: TokenStorageService) { }

  getAllMood() : Observable<Mood[]> {
    return this.httpClient.get<Mood[]>(this.baseURL, httpOptions);
  }

  addMood (mood? : Mood) : Observable<any> {
    console.log(this.otherURL + "/" + mood.moodModel + "/" + mood.budget + "/" + this.token.getUsername() + "/" + mood.location + "/" + mood.category + "/" + mood.physicalMood );
    return this.httpClient.post<any>(this.otherURL + "/" + mood.moodModel + "/" + mood.budget + "/" + this.token.getUsername() + "/" + mood.location + "/" + mood.category + "/" + mood.physicalMood , httpOptions);
  }

  getUserMood(userId : Number) : Observable<Mood[]> {
    return this.httpClient.get<Mood[]>(this.getUserMoodURL + "/" + userId);
  } 

}

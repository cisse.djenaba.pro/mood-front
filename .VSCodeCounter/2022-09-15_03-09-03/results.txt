Date : 2022-09-15 03:09:03
Directory : c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood
Total : 134 files,  43950 codes, 280 comments, 1624 blanks, all 45854 lines

Languages
+--------------------+------------+------------+------------+------------+------------+
| language           | files      | code       | comment    | blank      | total      |
+--------------------+------------+------------+------------+------------+------------+
| JSON               |          5 |     33,349 |          2 |          7 |     33,358 |
| SCSS               |         20 |      4,596 |        134 |        575 |      5,305 |
| TypeScript         |         82 |      2,786 |         86 |        599 |      3,471 |
| HTML               |         20 |      1,775 |          5 |        187 |      1,967 |
| JavaScript         |          5 |      1,401 |         52 |        240 |      1,693 |
| JSON with Comments |          1 |         29 |          1 |          2 |         32 |
| Markdown           |          1 |         14 |          0 |         14 |         28 |
+--------------------+------------+------------+------------+------------+------------+

Directories
+-------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                                              | files      | code       | comment    | blank      | total      |
+-------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                                                 |        134 |     43,950 |        280 |      1,624 |     45,854 |
| src                                                                                                               |        125 |     10,521 |        256 |      1,599 |     12,376 |
| src\app                                                                                                           |        115 |      9,082 |        162 |      1,329 |     10,573 |
| src\app\components                                                                                                |         83 |      8,366 |        144 |      1,152 |      9,662 |
| src\app\components\about-mood                                                                                     |          4 |         54 |          0 |         17 |         71 |
| src\app\components\activity                                                                                       |          4 |        623 |          6 |         45 |        674 |
| src\app\components\auth                                                                                           |         11 |        216 |          0 |         73 |        289 |
| src\app\components\calendar                                                                                       |          4 |        387 |          1 |         39 |        427 |
| src\app\components\colorbook                                                                                      |          4 |        184 |          0 |         30 |        214 |
| src\app\components\contact-form                                                                                   |          4 |        292 |          4 |         52 |        348 |
| src\app\components\event                                                                                          |          4 |        202 |          0 |         34 |        236 |
| src\app\components\header                                                                                         |          4 |        297 |          6 |         53 |        356 |
| src\app\components\home                                                                                           |          4 |        672 |          4 |         72 |        748 |
| src\app\components\images                                                                                         |          4 |        616 |         13 |         87 |        716 |
| src\app\components\login                                                                                          |          4 |        324 |         28 |         63 |        415 |
| src\app\components\login-icon                                                                                     |          4 |         76 |          0 |         18 |         94 |
| src\app\components\profile                                                                                        |          4 |        284 |          4 |         38 |        326 |
| src\app\components\register                                                                                       |          4 |        340 |          8 |         65 |        413 |
| src\app\components\restapi                                                                                        |          4 |      1,969 |         44 |        156 |      2,169 |
| src\app\components\sign-in                                                                                        |          4 |        700 |          8 |        127 |        835 |
| src\app\components\tracker                                                                                        |          4 |        462 |          2 |        115 |        579 |
| src\app\components\user                                                                                           |          4 |         46 |          0 |         12 |         58 |
| src\app\components\weather                                                                                        |          4 |        622 |         16 |         56 |        694 |
| src\app\models                                                                                                    |          7 |         69 |          0 |         13 |         82 |
| src\app\services                                                                                                  |         16 |        363 |          1 |        108 |        472 |
| src\assets                                                                                                        |          3 |      1,364 |         31 |        238 |      1,633 |
| src\assets\js                                                                                                     |          3 |      1,364 |         31 |        238 |      1,633 |
| src\environments                                                                                                  |          2 |          9 |         11 |          4 |         24 |
+-------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+-------------------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| filename                                                                                                          | language           | code       | comment    | blank      | total      |
+-------------------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\README.md                                                      | Markdown           |         14 |          0 |         14 |         28 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\angular.json                                                   | JSON               |        120 |          0 |          3 |        123 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\karma.conf.js                                                  | JavaScript         |         37 |          6 |          2 |         45 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\package-lock.json                                              | JSON               |     33,121 |          0 |          1 |     33,122 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\package.json                                                   | JSON               |         77 |          0 |          1 |         78 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\app-routing.module.ts                                  | TypeScript         |         44 |          0 |          7 |         51 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\app.component.html                                     | HTML               |          6 |          0 |          5 |         11 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\app.component.scss                                     | SCSS               |         31 |         17 |          7 |         55 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\app.component.spec.ts                                  | TypeScript         |         31 |          0 |          5 |         36 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\app.component.ts                                       | TypeScript         |         39 |          0 |         16 |         55 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\app.module.ts                                          | TypeScript         |        106 |          0 |          7 |        113 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\about-mood\about-mood.component.html        | HTML               |         11 |          0 |          5 |         16 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\about-mood\about-mood.component.scss        | SCSS               |         14 |          0 |          1 |         15 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\about-mood\about-mood.component.spec.ts     | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\about-mood\about-mood.component.ts          | TypeScript         |         11 |          0 |          5 |         16 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\activity\activity.component.html            | HTML               |        110 |          0 |          3 |        113 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\activity\activity.component.scss            | SCSS               |        462 |          6 |         28 |        496 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\activity\activity.component.spec.ts         | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\activity\activity.component.ts              | TypeScript         |         33 |          0 |          8 |         41 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\auth-interceptor.ts                    | TypeScript         |         26 |          0 |          6 |         32 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\auth.service.spec.ts                   | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\auth.service.ts                        | TypeScript         |         23 |          0 |         12 |         35 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\jwt-response.ts                        | TypeScript         |          7 |          0 |          1 |          8 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\login-info.ts                          | TypeScript         |          8 |          0 |          4 |         12 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\mood-info.ts                           | TypeScript         |         14 |          0 |          5 |         19 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\mood-storage.service.spec.ts           | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\mood-storage.service.ts                | TypeScript         |         16 |          0 |         11 |         27 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\signup-info.ts                         | TypeScript         |         22 |          0 |          2 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\token-storage.service.spec.ts          | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\auth\token-storage.service.ts               | TypeScript         |         64 |          0 |         17 |         81 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\calendar\calendar.component.html            | HTML               |        182 |          1 |          9 |        192 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\calendar\calendar.component.scss            | SCSS               |          3 |          0 |          3 |          6 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\calendar\calendar.component.spec.ts         | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\calendar\calendar.component.ts              | TypeScript         |        184 |          0 |         21 |        205 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\colorbook\colorbook.component.html          | HTML               |         80 |          0 |         12 |         92 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\colorbook\colorbook.component.scss          | SCSS               |         48 |          0 |          6 |         54 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\colorbook\colorbook.component.spec.ts       | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\colorbook\colorbook.component.ts            | TypeScript         |         38 |          0 |          6 |         44 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\contact-form\contact-form.component.html    | HTML               |         51 |          0 |          7 |         58 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\contact-form\contact-form.component.scss    | SCSS               |        190 |          4 |         29 |        223 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\contact-form\contact-form.component.spec.ts | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\contact-form\contact-form.component.ts      | TypeScript         |         33 |          0 |         10 |         43 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\event\event.component.html                  | HTML               |         81 |          0 |         10 |         91 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\event\event.component.scss                  | SCSS               |         73 |          0 |         11 |         84 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\event\event.component.spec.ts               | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\event\event.component.ts                    | TypeScript         |         30 |          0 |          7 |         37 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\header\header.component.html                | HTML               |         91 |          0 |          5 |         96 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\header\header.component.scss                | SCSS               |        166 |          6 |         36 |        208 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\header\header.component.spec.ts             | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\header\header.component.ts                  | TypeScript         |         22 |          0 |          6 |         28 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\home\home.component.html                    | HTML               |        126 |          0 |          9 |        135 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\home\home.component.scss                    | SCSS               |        438 |          4 |         45 |        487 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\home\home.component.spec.ts                 | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\home\home.component.ts                      | TypeScript         |         90 |          0 |         12 |        102 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\images\images.component.html                | HTML               |        258 |          0 |         23 |        281 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\images\images.component.scss                | SCSS               |        303 |         12 |         43 |        358 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\images\images.component.spec.ts             | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\images\images.component.ts                  | TypeScript         |         37 |          1 |         15 |         53 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\login-icon\login-icon.component.html        | HTML               |         10 |          0 |          4 |         14 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\login-icon\login-icon.component.scss        | SCSS               |          6 |          0 |          1 |          7 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\login-icon\login-icon.component.spec.ts     | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\login-icon\login-icon.component.ts          | TypeScript         |         42 |          0 |          7 |         49 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\login\login.component.html                  | HTML               |         43 |          0 |          7 |         50 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\login\login.component.scss                  | SCSS               |        181 |          8 |         36 |        225 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\login\login.component.spec.ts               | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\login\login.component.ts                    | TypeScript         |         82 |         20 |         14 |        116 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\profile\profile.component.html              | HTML               |         73 |          2 |          3 |         78 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\profile\profile.component.scss              | SCSS               |         63 |          2 |         12 |         77 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\profile\profile.component.spec.ts           | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\profile\profile.component.ts                | TypeScript         |        130 |          0 |         17 |        147 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\register\register.component.html            | HTML               |        100 |          0 |         14 |        114 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\register\register.component.scss            | SCSS               |        181 |          8 |         36 |        225 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\register\register.component.spec.ts         | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\register\register.component.ts              | TypeScript         |         41 |          0 |          9 |         50 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\restapi\restapi.component.html              | HTML               |        340 |          2 |         16 |        358 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\restapi\restapi.component.scss              | SCSS               |        994 |         40 |         76 |      1,110 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\restapi\restapi.component.spec.ts           | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\restapi\restapi.component.ts                | TypeScript         |        617 |          2 |         58 |        677 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\sign-in\sign-in.component.html              | HTML               |         29 |          0 |          5 |         34 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\sign-in\sign-in.component.scss              | SCSS               |        572 |          8 |        103 |        683 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\sign-in\sign-in.component.spec.ts           | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\sign-in\sign-in.component.ts                | TypeScript         |         81 |          0 |         13 |         94 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\tracker\tracker.component.html              | HTML               |        109 |          0 |         32 |        141 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\tracker\tracker.component.scss              | SCSS               |        311 |          2 |         60 |        373 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\tracker\tracker.component.spec.ts           | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\tracker\tracker.component.ts                | TypeScript         |         24 |          0 |         17 |         41 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\user\user.component.html                    | HTML               |          4 |          0 |          1 |          5 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\user\user.component.scss                    | SCSS               |          0 |          0 |          1 |          1 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\user\user.component.spec.ts                 | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\user\user.component.ts                      | TypeScript         |         24 |          0 |          4 |         28 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\weather\weather.component.html              | HTML               |         56 |          0 |          9 |         65 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\weather\weather.component.scss              | SCSS               |        537 |         16 |         36 |        589 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\weather\weather.component.spec.ts           | TypeScript         |         18 |          0 |          6 |         24 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\components\weather\weather.component.ts                | TypeScript         |         11 |          0 |          5 |         16 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\globals.ts                                             | TypeScript         |          9 |          0 |          2 |         11 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\horizontal-scroll.directive.spec.ts                    | TypeScript         |          7 |          0 |          2 |          9 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\horizontal-scroll.directive.ts                         | TypeScript         |         11 |          0 |          5 |         16 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\models\activity.ts                                     | TypeScript         |         10 |          0 |          4 |         14 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\models\events.model.ts                                 | TypeScript         |         23 |          0 |          0 |         23 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\models\mood.spec.ts                                    | TypeScript         |          6 |          0 |          2 |          8 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\models\mood.ts                                         | TypeScript         |          9 |          0 |          4 |         13 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\models\user.spec.ts                                    | TypeScript         |          6 |          0 |          2 |          8 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\models\user.ts                                         | TypeScript         |          4 |          0 |          1 |          5 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\models\weather.model.ts                                | TypeScript         |         11 |          0 |          0 |         11 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\activity.service.spec.ts                      | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\activity.service.ts                           | TypeScript         |         27 |          0 |          7 |         34 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\api.service.spec.ts                           | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\api.service.ts                                | TypeScript         |         55 |          0 |         16 |         71 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\contact.service.spec.ts                       | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\contact.service.ts                            | TypeScript         |         27 |          0 |          5 |         32 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\event-storage-service.service.spec.ts         | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\event-storage-service.service.ts              | TypeScript         |          8 |          0 |          6 |         14 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\event.service.spec.ts                         | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\event.service.ts                              | TypeScript         |         55 |          0 |          8 |         63 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\http-client.service.spec.ts                   | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\http-client.service.ts                        | TypeScript         |         40 |          1 |          7 |         48 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\mood.service.spec.ts                          | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\mood.service.ts                               | TypeScript         |         28 |          0 |         12 |         40 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\user.service.spec.ts                          | TypeScript         |         12 |          0 |          5 |         17 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\app\services\user.service.ts                               | TypeScript         |         27 |          0 |          7 |         34 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\assets\js\app.js                                           | JavaScript         |        664 |         11 |        114 |        789 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\assets\js\jl-coloringBook.js                               | JavaScript         |        492 |         18 |         69 |        579 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\assets\js\tracker.js                                       | JavaScript         |        208 |          2 |         55 |        265 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\environments\environment.prod.ts                           | TypeScript         |          4 |          0 |          1 |          5 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\environments\environment.ts                                | TypeScript         |          5 |         11 |          3 |         19 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\index.html                                                 | HTML               |         15 |          0 |          8 |         23 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\main.ts                                                    | TypeScript         |          9 |          0 |          4 |         13 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\polyfills.ts                                               | TypeScript         |          1 |         47 |          6 |         54 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\styles.scss                                                | SCSS               |         23 |          1 |          5 |         29 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\src\test.ts                                                    | TypeScript         |         18 |          4 |          5 |         27 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\tsconfig.app.json                                              | JSON               |         14 |          1 |          1 |         16 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\tsconfig.json                                                  | JSON with Comments |         29 |          1 |          2 |         32 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\tsconfig.spec.json                                             | JSON               |         17 |          1 |          1 |         19 |
| c:\Users\cisse\OneDrive\Bureau\Projet 4\front\mood\webpack.config.js                                              | JavaScript         |          0 |         15 |          0 |         15 |
| Total                                                                                                             |                    |     43,950 |        280 |      1,624 |     45,854 |
+-------------------------------------------------------------------------------------------------------------------+--------------------+------------+------------+------------+------------+
import { TestBed } from '@angular/core/testing';

import { EventStorageServiceService } from './event-storage-service.service';

describe('EventStorageServiceService', () => {
  let service: EventStorageServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventStorageServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

# Summary

Date : 2022-09-14 17:33:17

Directory c:\\Users\\cisse\\OneDrive\\Bureau\\Projet 4\\front\\mood

Total : 130 files,  43594 codes, 292 comments, 1690 blanks, all 45576 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 5 | 33,349 | 2 | 7 | 33,358 |
| SCSS | 19 | 4,681 | 146 | 593 | 5,420 |
| TypeScript | 80 | 2,706 | 86 | 593 | 3,385 |
| HTML | 19 | 1,427 | 6 | 244 | 1,677 |
| JavaScript | 5 | 1,388 | 51 | 237 | 1,676 |
| JSON with Comments | 1 | 29 | 1 | 2 | 32 |
| Markdown | 1 | 14 | 0 | 14 | 28 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 130 | 43,594 | 292 | 1,690 | 45,576 |
| src | 121 | 10,165 | 268 | 1,665 | 12,098 |
| src\\app | 111 | 8,748 | 175 | 1,400 | 10,323 |
| src\\app\\components | 79 | 8,023 | 157 | 1,219 | 9,399 |
| src\\app\\components\\about-mood | 4 | 51 | 0 | 18 | 69 |
| src\\app\\components\\activity | 4 | 609 | 6 | 44 | 659 |
| src\\app\\components\\auth | 11 | 216 | 0 | 73 | 289 |
| src\\app\\components\\calendar | 4 | 387 | 1 | 39 | 427 |
| src\\app\\components\\colorbook | 4 | 177 | 0 | 28 | 205 |
| src\\app\\components\\contact-form | 4 | 289 | 4 | 52 | 345 |
| src\\app\\components\\event | 4 | 311 | 13 | 58 | 382 |
| src\\app\\components\\header | 4 | 298 | 6 | 53 | 357 |
| src\\app\\components\\home | 4 | 637 | 4 | 75 | 716 |
| src\\app\\components\\images | 4 | 464 | 13 | 102 | 579 |
| src\\app\\components\\login | 4 | 324 | 28 | 63 | 415 |
| src\\app\\components\\profile | 4 | 281 | 4 | 37 | 322 |
| src\\app\\components\\register | 4 | 340 | 8 | 65 | 413 |
| src\\app\\components\\restapi | 4 | 1,809 | 44 | 202 | 2,055 |
| src\\app\\components\\sign-in | 4 | 700 | 8 | 127 | 835 |
| src\\app\\components\\tracker | 4 | 462 | 2 | 115 | 579 |
| src\\app\\components\\user | 4 | 46 | 0 | 12 | 58 |
| src\\app\\components\\weather | 4 | 622 | 16 | 56 | 694 |
| src\\app\\models | 7 | 69 | 0 | 13 | 82 |
| src\\app\\services | 16 | 359 | 1 | 108 | 468 |
| src\\assets | 3 | 1,351 | 30 | 235 | 1,616 |
| src\\assets\\js | 3 | 1,351 | 30 | 235 | 1,616 |
| src\\environments | 2 | 9 | 11 | 4 | 24 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)
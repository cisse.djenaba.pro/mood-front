import { Component, OnInit } from '@angular/core';
import { MoodStorageService } from '../auth/mood-storage.service';
import { MoodService } from 'src/app/services/mood.service';
import { Mood } from 'src/app/models/mood';
declare const loadTracker: any; 


@Component({
  selector: 'app-tracker',
  templateUrl: './tracker.component.html',
  styleUrls: ['./tracker.component.scss']
})



export class TrackerComponent implements OnInit {

  

  constructor(private moodStorageService : MoodStorageService, private moodService : MoodService) { }


  savedMood = this.moodStorageService.getSavedMood();
  moods = this.moodService.getAllMood(); 

  public _moods : Mood[] = []; 
  
  ngOnInit(): void {
   loadTracker(); 
  }



  getMoods() {
    this.moodService.getAllMood().subscribe((data) => {
      this._moods = data; 
    })
  }

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MoodStorageService } from '../auth/mood-storage.service';

import { TokenStorageService } from '../auth/token-storage.service';

@Component({
  selector: 'app-home',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  info: any;

  constructor(
    private token: TokenStorageService,
    private moodStorageService: MoodStorageService
  ) {}

  savedMood = this.moodStorageService.getSavedMood();
  moodString: string = 'humeur';

  ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities(),
    };
  }

  logout() {
    this.token.signOut();
    window.location.reload();
  }

  replaceMood() {
    console.log('tu rentre dans la méthode replaceMood');
    switch (this.savedMood.moodModel) {
      case 'MOOD_VERYGOOD':
        return (this.savedMood.moodModel = 'Très bonne humeur');
      case 'MOOD_GOOD':
        return (this.savedMood.moodModel = 'Bonne humeur');
      case 'MOOD_NEUTRAL':
        return (this.savedMood.moodModel = 'Humeur neutre');
      case 'MOOD_BAD':
        return (this.savedMood.moodModel = 'Mauvaise humeur');
      case 'MOOD_VERYBAD':
        return (this.savedMood.moodModel = 'Très mauvaise humeur');
      default:
        return 'humeur';
    }
  }

  replacePhysicalMood() {
    switch (this.savedMood.physicalMood) {
      case 'MOOD_1':
        return (this.savedMood.physicalMood = 'Très mauvaise');
      case 'MOOD_2':
        return (this.savedMood.physicalMood = 'Mauvaise');
      case 'MOOD_3':
        return (this.savedMood.physicalMood = 'Neutre');
      case 'MOOD_4':
        return (this.savedMood.physicalMood = 'Bonne');
      case 'MOOD_5':
        return (this.savedMood.physicalMood = 'Très bonne');
      default:
        return 'humeur';
    }
  }

  replaceLocationMood() {
    switch (this.savedMood.location) {
      case 'MOOD_IN':
        return (this.savedMood.location = "à rester à l'Intérieur");
      case 'MOOD_IDC':
        return (this.savedMood.location = 'indifférente au lieu');
      case 'MOOD_OUT':
        return (this.savedMood.location = "à profiter de l'Extérieur");

      default:
        return 'humeur';
    }
  }

  replaceCategoryMood() {
    switch (this.savedMood.category) {
      case 'MOOD_MUSIC':
        return (this.savedMood.category = 'musicale');
      case 'MOOD_SPORT':
        return (this.savedMood.category = 'sportive');
      case 'MOOD_MEDITATION':
        return (this.savedMood.category = 'méditative');
      case 'MOOD_CULTURE':
        return (this.savedMood.category = 'culturelle');
      case 'MOOD_ART':
        return (this.savedMood.category = 'artistique');
      default:
        return 'humeur';
    }
  }
}
